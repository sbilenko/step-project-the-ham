import { GALLERY } from './section-work_gallery.js'

export const SECTION_WORK = () => {


  const workTabs = document.querySelectorAll('.work__tabs-item')
  const loadButton = document.querySelector('.work__button-wrapper')
  const itemsWrapper = document.querySelector('.work__tabs-content')

  const drawToHtml = () => {
    itemsWrapper.innerHTML = '';
    GALLERY.sort(() => Math.random() - 0.5).forEach((item) => {
      itemsWrapper.insertAdjacentHTML(
        'beforeend',
        `
    <div class="content-hover-item--wrapper">
      <img src="${item.src}" class="work__content-item ${item.category.split(' ').join('')}"></img>
      <div class="work__content-hover-item">
        <img src="./images/section_work/work__content-hover-item.svg" alt="">
        <div class="hover-item__texts">
          <h5 class="hover-item__title">Creative Design</h5>
          <span class="hover-item__text">${item.category}</span>
        </div>
      </div>
    </div>
    `
      )
    })
  }
  drawToHtml()

  let workContentItems;
  const findItems = () => workContentItems = document.querySelectorAll('.work__content-item');
  findItems();


  const loading = (collection, limit) => {
    [...collection].slice(0, limit).forEach((item) => {
      item.parentNode.classList.add('show')
    })
  }
  loading(workContentItems, 12)

  const firstCheckButton = (array, button) => {
    if (array.length < 12) {
      button.classList.add('hide')
    } else {
      button.classList.remove('hide')
    }
  }

  let filteredArray = workContentItems
  const createFilteredArray = (array, filter) => {
    filteredArray = []
    array.forEach((el) => {
      el.parentNode.classList.remove('show')
      if (el.classList.contains(filter)) {
        filteredArray.push(el)
      } else if (filter === 'All') {
        el.parentNode.classList.remove('show')
        filteredArray = [...workContentItems]
        loading(filteredArray, 12)
      } else {
        el.classList.remove('show')
      }
    })

    firstCheckButton(filteredArray, loadButton)
    loading(filteredArray, 12)
  }

  workTabs.forEach((tab) => {
    tab.addEventListener('click', (e) => {
      workTabs.forEach((tab) => {
        tab.classList.remove('work__tabs-item-active')
      })

      drawToHtml()
      findItems()

      e.target.classList.add('work__tabs-item-active')
      let filterClass = e.target.dataset.filter.split(' ').join('')
      createFilteredArray(workContentItems, filterClass)
    })
  })

  let countClick = 0
  loadButton.addEventListener('click', () => {
    if (countClick === 0 && filteredArray.length > 24) {
      loading(filteredArray, 24)
      countClick++
    } else if (countClick === 0 && filteredArray.length <= 24) {
      loading(filteredArray, 24)
      loadButton.classList.add('hide')
      countClick = 0
    } else {
      loading(filteredArray, 36)
      countClick = 0
      loadButton.classList.add('hide')
    }
  })


}


export const SLIDER = () => {

  const bulletsWrapper = document.querySelector('.swiper-pagination')
  const btnPrev = document.querySelector('.swipe-button-prev')
  const btnNext = document.querySelector('.swipe-button-next')

  

  let move = 0
  btnNext.addEventListener('click', () => {
    move += 90
    if (move > 180) {
      move = 180 
    }
    bulletsWrapper.style.left = `${-move}px`
  })

  btnPrev.addEventListener('click', () => {
    move -= 90
    if (move < 0) {
      move = 0
    }
    bulletsWrapper.style.left = `${-move}px`
  })
  
  

  let slider = new Swiper('.swiper', {
    slidesPerView: 1,
    speed: 500,
    // loop: true,
    // navigation: {
    //   nextEl: '.swipe-button-next',
    //   prevEl: '.swipe-button-prev',
    // },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  })

}

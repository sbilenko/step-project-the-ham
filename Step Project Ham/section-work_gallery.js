export const GALLERY = [
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design1.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design2.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design3.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design4.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design5.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design6.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design7.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design8.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design9.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design10.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design12.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design5.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design7.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design3.jpg'
  },
  {
    category: 'Graphic Design',
    src: './images/section_work/graphic_design/graphic-design9.jpg'
  },
  {
    category: 'Web Design',
    src: './images/section_work/web_design/web-design1.jpg'
  },
  {
    category: 'Web Design',
    src: './images/section_work/web_design/web-design2.jpg'
  },
  {
    category: 'Web Design',
    src: './images/section_work/web_design/web-design3.jpg'
  },
  {
    category: 'Web Design',
    src: './images/section_work/web_design/web-design4.jpg'
  },
  {
    category: 'Web Design',
    src: './images/section_work/web_design/web-design5.jpg'
  },
  {
    category: 'Web Design',
    src: './images/section_work/web_design/web-design6.jpg'
  },
  {
    category: 'Web Design',
    src: './images/section_work/web_design/web-design7.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page1.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page2.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page3.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page4.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page5.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page6.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page7.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page1.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page3.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page2.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page6.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page7.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page5.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page7.jpg'
  },
  {
    category: 'Landing Pages',
    src: './images/section_work/landing_page/landing-page2.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress1.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress2.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress3.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress4.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress5.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress6.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress7.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress8.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress9.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress10.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress3.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress5.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress1.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress7.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress10.jpg'
  },
  {
    category: 'Wordpress',
    src: './images/section_work/wordpress/wordpress2.jpg'
  },
]
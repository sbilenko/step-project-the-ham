export const SECTION_SERVICE = () => {

  
  const serviceTabs = document.querySelectorAll('.service__tabs-item')
  const serviceContentItems = document.querySelectorAll(
    '.service__content-item'
  )

  serviceTabs.forEach((tab) => {
    tab.addEventListener('click', (e) => {
      const currentTabDataNumber = e.target.dataset.number

      serviceTabs.forEach((tab) => {
        tab.classList.remove('service__tabs-item-active')
      })

      serviceContentItems.forEach((item) => {
        item.classList.add('hidden')
      })

      const currentContentItem = document.querySelector(
        `.item-${currentTabDataNumber}`
      )

      currentContentItem.classList.remove('hidden')
      e.target.classList.add('service__tabs-item-active')

    })
  })
  
  
}
